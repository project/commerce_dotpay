/**
 * @file
 * Contains commerce_dotpay payment channels behaviours.
 */

(function (Drupal, $) {
  Drupal.behaviors.commerce_dotpay_payment_channels = {
    attach: function (context, settings) {
      var paymentButtons = 'button[data-payment-channel]';
      var activeClass = 'active';

      var $checkedRadio = $('.payment-channels-wrapper input:checked');

      if ($checkedRadio.length) {
        $(paymentButtons)
          .filter('[data-payment-channel="' + $checkedRadio.val() + '"]')
          .addClass(activeClass);
      }

      $(paymentButtons).click(function () {
        var $radio = $('.payment-channels-wrapper input[value="' + $(this).data('payment-channel') + '"]');
        $radio
          .prop("checked", true)
          .trigger("click");

        $(paymentButtons).removeClass(activeClass);
        $(this).addClass(activeClass);
      });
    }
  };
})(Drupal, jQuery);
