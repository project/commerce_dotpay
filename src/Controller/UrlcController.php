<?php

namespace Drupal\commerce_dotpay\Controller;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\commerce_dotpay\DotpayAPIOperations;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controller for handling response from Dotpay.
 *
 * @package Drupal\commerce_dotpay\Controller
 */
class UrlcController extends ControllerBase {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $logger;

  /**
   * Dotpay api operations service.
   *
   * @var \Drupal\commerce_dotpay\DotpayAPIOperations
   */
  protected $dotpayApi;

  /**
   * {@inheritDoc}
   */
  public function __construct(EntityTypeManager $entityTypeManager, LoggerChannelFactory $loggerChannelFactory, DotpayAPIOperations $dotpayApi) {
    $this->entityTypeManager = $entityTypeManager;
    $this->logger = $loggerChannelFactory->get('commerce_dotpay');
    $this->dotpayApi = $dotpayApi;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('logger.factory'),
      $container->get('commerce_dotpay.api_operations')
    );
  }

  /**
   * Returns response specified in dotpay docs, and sets order completion
   * status.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request parameters.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Controller's response.
   */
  public function returnStatus(Request $request) {
    $response = new Response();
    $response->setContent('ERROR');
    $requestParams = $this->getRequestParams($request->request);
    $configuration = $this->getConfiguration();
    $signature = $this->dotpayApi->generateSignature($configuration['pin'], $requestParams);
    if ($signature == $requestParams['signature']) {
      $orderId = $requestParams['control'];
      $payment = $this->loadPayment($orderId, $requestParams['operation_original_amount']);
      if (!empty($payment)) {
        $payment->setRemoteId($requestParams['operation_number']);
        $payment->setRemoteState($requestParams['operation_status']);
        $payment->setState($requestParams['operation_status']);
        try {
          $payment->save();
          $response->setContent('OK');
        }
        catch (EntityStorageException $e) {
          $this->logger->error($e->getMessage());
        }
      }
      else {
        $this->logger->error("No payment created for order ID: {$requestParams['control']}");
      }
    }
    else {
      $this->logger->error("Wrong checksum for order ID: {$requestParams['control']}");
    }

    return $response;
  }

  /**
   * Loads payment entity assocaited with order.
   *
   * @param $orderId
   *   Order ID.
   * @param $amount
   *   Payment amount from dotpay.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   Returns payment entity or null.
   */
  private function loadPayment($orderId, $amount) {
    $payments = $this->entityTypeManager->getStorage('commerce_payment')
      ->loadByProperties(
        [
          'order_id' => $orderId,
          'remote_id' => $orderId . 'pending',
          'remote_state' => 'new',
          'state' => 'new',
        ]
      );
    foreach ($payments as $payment) {
      /**
       * @var \Drupal\commerce_payment\Entity\Payment
       */
      /**
       * @var \Drupal\metatag_open_graph_products\Plugin\metatag\Tag\ProductPriceAmount
       */
      $orderAmount = $payment->getAmount()->getNumber();
      if ($orderAmount == $amount) {
        return $payment;
      }
    }

    return NULL;
  }

  /**
   * Loads order associated with the payment.
   *
   * @param int $orderId
   *   Order Id sent in the control value of request from dotpay.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   Order entity, or null if it doesn't exist.
   */
  private function loadOrder($orderId) {
    $order = NULL;
    try {
      $order = $this->entityTypeManager->getStorage('commerce_order')
        ->load($orderId);
    }
    catch (InvalidPluginDefinitionException $e) {
      $this->logger->error($e->getMessage());
    }
    catch (PluginNotFoundException $e) {
      $this->logger->error($e->getMessage());
    }

    return $order;
  }

  /**
   * Returns gateway configuration.
   *
   * @return mixed
   *   Gateway configuration.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function getConfiguration() {
    $gateway = $this->entityTypeManager->getStorage('commerce_payment_gateway')
      ->loadByProperties([
        'plugin' => 'commerce_dotpay_checkout',
      ]);
    return reset($gateway)->getPluginConfiguration();
  }

  /**
   * Returns array containing request parameters.
   *
   * @param \Symfony\Component\HttpFoundation\ParameterBag $requestData
   *   Request data.
   *
   * @return array
   *   Array with request parameters $parameter_name => $value.
   */
  private function getRequestParams(ParameterBag $requestData) {
    $params = [];
    foreach ($requestData as $key => $requestParam) {
      $params[$key] = $requestParam;
    }

    return $params;
  }

}
