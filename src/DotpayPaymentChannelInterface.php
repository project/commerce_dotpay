<?php

namespace Drupal\commerce_dotpay;

/**
 * Provides interface for dotpay payment channels.
 *
 * @package Drupal\commerce_dotpay
 */
interface DotpayPaymentChannelInterface {

  /**
   * Blik payment channel id.
   */
  const BLIK = 73;

}
