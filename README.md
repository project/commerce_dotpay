CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

This module allows to use Dotpay and Dotpay Blik payment gateways (https://www.dotpay.pl/).

REQUIREMENTS
------------

This module adds Dotpay payment gateways to Drupal commerce,
so it requires commerce and commerce_payment modules.
You need to download/install them first in order to use this module.

INSTALLATION
------------

Install normally as other modules are installed. For Support:
https://www.drupal.org/docs/8/extending-drupal/installing-contributed-modules

CONFIGURATION
----------------------
Go to /admin/commerce/config/payment-gateways and edit Blik or Dotpay gateway.
By default both gateways use test mode, so in order to use it in production
mode, you will have to change "Mode" value to "Live".

Go to admin/commerce/config/checkout-flows and edit your checkout flow, ex:
/admin/commerce/config/checkout-flows/manage/shipping
By default "Dotpay checkout" is placed under the "Order Information", you can
customize it by drag&drop to the preferred step.
You will have to verify that it is placed on the same step and below
the "Payment information" pane.

You can find PIN and Seller ID in your Dotpay panel.
You will have to set the Urlc in your Dotpay panel configuration with
the following pattern:
{{ your site URL }}/dotpay/urlc ex.: https://example.com/dotpay/urlc

MAINTAINERS:
----------------------
Maintainers:
Tomasz Makiel https://www.drupal.org/u/tmakiel
Mariusz Andrzejewski https://drupal.org/u/sayco
Piotr Kamieniecki https://www.drupal.org/u/piotrkamieniecki
